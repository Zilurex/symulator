#include "stdafx.h"
//#include "Map.h"
#include "Character.h"
#include <iostream>
using namespace std;
map::cell::cell()
{
	type = 0;
}
map::map()
{
	rowCount = 10;
	colCount = 10;
	cell* p;
	for (int i = 0; i < rowCount; i++)
	{
		for (int j = 0; j < colCount; j++)
		{
			p = new cell;
			this->field[i][j] = p;
		}
	}
}
map::~map()
{

	for (int i = 0; i < rowCount; i++)
	{
		for (int j = 0; j < colCount; j++)
		{
			delete this->field[i][j];
		}
	}
}
void map::display()
{

	for (int i = 0; i < rowCount; i++)
	{
		for (int j = 0; j < colCount; j++)
		{
			gotoxy(i, j);
			if (this->field[i][j]->type == 0)
			{
				cout << " ";
			}
			else if (this->field[i][j]->type == 1)
			{
				cout << "#";
			}


		}
	}
}

void ShowConsoleCursor(bool showFlag)
{
	HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);

	CONSOLE_CURSOR_INFO     cursorInfo;

	GetConsoleCursorInfo(out, &cursorInfo);
	cursorInfo.bVisible = showFlag; // set the cursor visibility
	SetConsoleCursorInfo(out, &cursorInfo);
}